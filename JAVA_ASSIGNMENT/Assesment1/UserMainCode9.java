package com.Assesment1;

import java.util.Scanner;

class UserMain{
	public static int sumOfSquaresOfEvenDigits(int number){
		int rem;
		int sqsum=0;
		if (number>0){
			while (number>0){
				rem=number%10;
				number=number/10;
				if (rem%2==0){
					sqsum=sqsum+(rem*rem);
				}
			}
			if(sqsum==0){
				return 1;
			}
			else{
				return sqsum;
			}
		}
		else {
			return 0;
		}
	}
}
public class UserMainCode9 {
	public static void main(String [] args){
		
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter positive number:");
		int n=sc.nextInt();
		
		int result=UserMain.sumOfSquaresOfEvenDigits(n);
		if (result==0){
			System.out.println("Enter a positive number");
		}
		else if(result==1){
			System.out.println("No even digits. 0");
		}
		else{
			System.out.println("Sum of square of the even digits is:: "+result);
			sc.close();
		}
	}

}