package com.Assesment1;

public class Swap4 {

	public static void main(String[] args) {
		
		
		// with third variable 
		/*
		 * int a=20,b=30; int t; t=a; a=b; b=t;
		 */
		// without using third variable
		int a=20,b=30;
		  a=a+b;
		  b=a-b;
		  a=a-b;
		System.out.println("A is:"+a);
		System.out.println("B is:"+b);

	}

}
