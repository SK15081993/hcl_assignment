package com.Assesment5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

   class RunRate{
	   
	RunRate(int total_runs_scored, int total_overs_faced){
	
		try {
			float runRate = total_runs_scored/total_overs_faced;
			System.out.println("Current Run Rate :"+runRate);
		}
		catch (Exception e)
		{
			System.out.println("java.lang.ArithmeticException");
		}
	}
	}

 class Cricket5 {

	public static void main(String[] args) throws NumberFormatException, IOException {

		  BufferedReader reader =new BufferedReader(new InputStreamReader(System.in));
	   
	      System.out.println("Enter the total runs scored:: ");
	      int run = Integer.parseInt(reader.readLine());
	      System.out.println("Enter the total overs faced::");
	      int over = Integer.parseInt(reader.readLine());
	      
	      RunRate runRate=new RunRate(run,over);
	 	}
}
