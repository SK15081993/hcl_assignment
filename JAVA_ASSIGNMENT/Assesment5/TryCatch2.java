package com.Assesment5;

public class TryCatch2 {

	public static void main(String[] args) {

    System.out.println("Main Started");
    
    try {
    	String data=args[0];
    	String data1=args[1];
    	
    	int x=Integer.parseInt(data);
    	int y=Integer.parseInt(data1);
    	
    	int result=x/y;
    	System.out.println("Result:"+result);	
    }
    catch(ArrayIndexOutOfBoundsException|NumberFormatException|ArithmeticException e) {
    	System.out.println("Provide one non zero int value as CLA");
    }
    System.out.println("Main completed");
	}

}
