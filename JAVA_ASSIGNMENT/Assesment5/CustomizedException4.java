package com.Assesment5;

import java.util.Scanner;

      class CustomException4 extends Exception{

/**
	 * 
	 */
	     private static final long serialVersionUID = 1L;

            CustomException4(String s){
            	super(s);
            }

      }
   class CustomizedException{
	   
   public static void main(String[] args) throws CustomException4{

        Scanner sc=new Scanner(System.in);
        System.out.println("Enter player name:");
             String name=sc.nextLine();
        System.out.println("Enter player age:");
            int age=sc.nextInt();
              if(age<19) {
            	  
                throw new CustomException4("InvalidAgeRangeException");
                }
              else {
	            System.out.println("Player name:"+name);
	            System.out.println("Player age:"+age);
              }
   }
}