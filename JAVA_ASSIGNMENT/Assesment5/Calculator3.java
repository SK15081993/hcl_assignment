package com.Assesment5;

import java.util.Scanner;

class MyCalculator {
	
    public long power(int n,int p) throws Exception
    {
        if(n==0&&p==0)
            throw new Exception("n and p should not be zero");
        else if(n < 0 || p < 0)
            throw new Exception("n or p should not be negative");
        else
            return (long)(Math.pow(n,p));
    } 
}
 public class Calculator3 {
	 
    public static final MyCalculator mc = new MyCalculator();
    public static final Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args) {
        while (sc.hasNextInt()) {
            int n=sc.nextInt();
            int p=sc.nextInt();
            
            try {
                System.out.println(mc.power(n,p));
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }
}