package com.pet.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.databaseconnection.DBConnection;
import com.pet.model.Pet;
import com.pet.model.User;

public class PetDaoImpl implements PetDao {
	
	Connection con=null;
	PreparedStatement ps=null;
	public int insertPet(Pet pet) throws Exception {
		con=DBConnection.getConnection();
		String query="insert into pet(petname,petage,petplace)values(?,?,?)";
		ps=con.prepareStatement(query);
		ps.setString(1,pet.getPetname());
		ps.setInt(2,pet.getPetage());
		ps.setString(3,pet.getPetplace());
		
       int result=ps.executeUpdate();
        System.out.println(result+"inserted pet successfully");
		return result;
	}

	@Override
	public ArrayList<Pet> getAllPet() throws Exception {
		
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		  ArrayList<Pet> pets= new ArrayList<>();
			con=DBConnection.getConnection();
			String query="select petname,petage,petplace from pet";
			ps=con.prepareStatement(query);
			 rs = ps.executeQuery();
	         while(rs.next()) {
	        	 Pet pet=new Pet();
	        	 pet.setPetname(rs.getString(1).toString());
	        	 pet.setPetage(rs.getInt(2));
	        	 pet.setPetplace(rs.getString(3));
	        	 
	        	 pets.add(pet);
	         }
	         
              return pets;
	      }

	
	public boolean updatePet(int userid, int petid)  {
		boolean status=false;
		Connection con=null;
		PreparedStatement ps=null;
		try {
			con=DBConnection.getConnection();
			ps=con.prepareStatement("update pet set petownerid=? where petid=?");
			ps.setInt(1, userid);
			ps.setInt(2, petid);
			int i=ps.executeUpdate();
			if(i>0) {
				status=true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public int deletePet(String petid) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ArrayList<Pet> myPet(int userid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUser(String username) {
		// TODO Auto-generated method stub
		return null;
	}
	
	   }
