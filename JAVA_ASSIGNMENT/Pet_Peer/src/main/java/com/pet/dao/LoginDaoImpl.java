package com.pet.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.databaseconnection.DBConnection;
import com.pet.model.Login;

public class LoginDaoImpl implements LoginDao {

	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	boolean status = false;

	public boolean validate(Login login) throws Exception {
		con = DBConnection.getConnection();

		String query = "select *from login where username=? and password=?";
		ps = con.prepareStatement(query);

		ps.setString(1, login.getUsername());
		ps.setString(2, login.getPassword());
		rs = ps.executeQuery();
		status = rs.next();
		return status;
	}

}
