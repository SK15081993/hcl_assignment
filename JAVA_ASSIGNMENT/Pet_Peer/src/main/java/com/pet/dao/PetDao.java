package com.pet.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pet.model.Pet;
import com.pet.model.User;

public interface PetDao {

	public int insertPet(Pet pet) throws Exception;
	public boolean updatePet(int userid,int petid) throws SQLException;
	public int deletePet(String petid);
	public ArrayList<Pet> myPet(int userid);
	public ArrayList<Pet> getAllPet() throws Exception;
	public User getUser(String username);
	
}
