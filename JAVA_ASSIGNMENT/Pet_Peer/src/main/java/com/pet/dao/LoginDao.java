package com.pet.dao;

import com.pet.model.Login;

public interface LoginDao {

	public boolean validate(Login login) throws Exception;
}
