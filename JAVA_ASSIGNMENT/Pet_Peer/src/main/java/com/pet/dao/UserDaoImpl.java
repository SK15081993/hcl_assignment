package com.pet.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

import com.databaseconnection.DBConnection;
import com.pet.model.UserRegistration;

public class UserDaoImpl implements UserDao{

	Connection con=null;
	PreparedStatement ps=null;
	@Override
	public int insertUser(UserRegistration ur) throws Exception {
		con=DBConnection.getConnection();
		
		String query="insert into userRegistration(username,password,confirmpassword) values(?,?,?)";
		ps=con.prepareStatement(query);
		ps.setString(1,ur.getUsername());
		ps.setString(2,ur.getPassword());
		ps.setString(3,ur.getConfirmpassword());
        int result=ps.executeUpdate();
        System.out.println(result+"updated");
		return result;
		
	}

	/*
	 * @Override public int updateUser(UserRegistration ur) { // TODO Auto-generated
	 * method stub return 0; }
	 * 
	 * @Override public int deleteUser(String username) { // TODO Auto-generated
	 * method stub return 0; }
	 */

}
