package com.pet.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pet.dao.PetDaoImpl;
import com.pet.model.Pet;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PetDaoImpl dao = new PetDaoImpl();
		try {
			   ArrayList<Pet> listofPet=dao.getAllPet();
			request.setAttribute("petlist",listofPet);
			RequestDispatcher rd = request.getRequestDispatcher("/home.jsp");
			rd.forward(request,response);
		}
		
		catch(Exception e) {
			PrintWriter out = response.getWriter();
			out.println("Something went gone");
		}
		
		
	}

	
	

}
