package com.pet.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pet.dao.PetDaoImpl;


@WebServlet("/BuyPetServlet")
public class BuyPetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter out=response.getWriter();
		int petid=Integer.parseInt(request.getParameter("petid"));
		int userid=Integer.parseInt(request.getParameter("userid"));
		
		PetDaoImpl dao=new PetDaoImpl();
		if(dao.updatePet(userid, petid)) {
			RequestDispatcher rd=request.getRequestDispatcher("home.jsp");
			rd.include(request, response);
	}
}	}	