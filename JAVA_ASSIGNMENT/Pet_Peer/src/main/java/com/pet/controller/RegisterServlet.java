package com.pet.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pet.dao.UserDaoImpl;
import com.pet.model.UserRegistration;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String confirmpassword=request.getParameter("confirmpassword");
	
		UserRegistration ur=new UserRegistration();
		 UserDaoImpl udl=new UserDaoImpl();
		
		if (username.isEmpty() || password.isEmpty() || confirmpassword.isEmpty()) {	
            RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
            out.println("<font color=red>Please fill all the fields</font>");
    		
            rd.include(request, response);
        } else {
        	if(!password.equals(confirmpassword)) {
				    RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
		            out.println("<font color=red>password and confirmpassword should be same</font>");
		            rd.include(request, response);
			}
        	else {
        		 ur.setUsername(username);
                 ur.setPassword(password);
                 ur.setConfirmpassword(confirmpassword);
                 int res=0;
                 try {
    				res=udl.insertUser(ur);

    			} catch (Exception e) {
    				 RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
    				 out.println("<font color=red>This username is taken</font>");
    		            out.println("<font color=red>Please use diffrent Username</font>");
    		            rd.include(request, response);
    				e.printStackTrace();
    			}
  
                 if(res>0) {
                RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
                rd.forward(request, response);
                 }
        	}
        	
        }
    }

	}
