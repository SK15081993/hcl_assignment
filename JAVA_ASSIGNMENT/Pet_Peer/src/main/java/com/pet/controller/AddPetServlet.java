package com.pet.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.pet.dao.PetDaoImpl;
import com.pet.model.Pet;

@WebServlet("/AddPetServlet")
public class AddPetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		int petage = 0;
		String petname = request.getParameter("petname");// null
		if (!request.getParameter("age").isEmpty()) {
			petage = Integer.parseInt(request.getParameter("age")); // null
		}
		String petplace = request.getParameter("place"); // null

		Pet pet = new Pet();
		PetDaoImpl pdaoimpl = new PetDaoImpl();

		if (petname.isEmpty() || petplace.isEmpty()) {

			RequestDispatcher rd = request.getRequestDispatcher("addpet.jsp");
			out.println("<font color=red>Please fill all the fields</font>");
			rd.include(request, response);
		}

		else if (petage >= 1 && petage <= 98) {

			pet.setPetname(petname);
			pet.setPetage(petage);
			pet.setPetplace(petplace);
			int result = 0;
			try {
				result = pdaoimpl.insertPet(pet);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (result > 0) {
				RequestDispatcher rd = request.getRequestDispatcher("home.jsp");
				rd.forward(request, response);
			}

		} else {
			RequestDispatcher rd = request.getRequestDispatcher("addpet.jsp");
			rd.include(request, response);
			out.println("<font color=red> age should be 0 to 99</font>");
		}
	}
}
