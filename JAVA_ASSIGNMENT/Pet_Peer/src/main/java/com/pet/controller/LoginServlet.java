package com.pet.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pet.dao.LoginDaoImpl;
import com.pet.model.Login;


@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		
		Login login=new Login();
		LoginDaoImpl ldao=new LoginDaoImpl();
		
		if (username.isEmpty() || password.isEmpty()) {
			 RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
	            out.println("<font color=red>Please fill all the fields</font>");
	            rd.include(request, response);
		}else {
        	if(!username.equals(password)) {
				    RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
		            out.println("<font color=red>username and password should be same</font>");
		            rd.include(request, response);
			}
        	
        	else {
        		login.setUsername(username);
        		login.setPassword(password);
      
		try {
			if(ldao.validate(login)) {
				response.sendRedirect("homePage.jsp");
			}else {
				response.sendRedirect("login.jsp");
			}

		
	}catch(Exception e) {
		e.printStackTrace();
	}
        	}
}
	}}	