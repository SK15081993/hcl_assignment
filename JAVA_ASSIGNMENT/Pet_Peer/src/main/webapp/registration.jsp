<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Registration</title>
</head>
<body align="center">
	<h3>Pet Peer Registration</h3>
	<form action="RegisterServlet" method="post">
		<table border="1">
			<tr>
				<td>Username</td>
				<td><input type="text" name="username"></td>
			</tr>

			<tr>
				<td>Password</td>
				<td><input type="password" name="password"></td>
			</tr>
			<tr>
				<td>Confirm Password</td>
				<td><input type="password" name="confirmpassword"></td>
			<tr>
				<td><input type="submit" value="Register"></td>
			</tr>
		</table>
		<br /> <a href="login.jsp">Login</a>
	</form>
</body>
</html>