<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Pet</title>
</head>
<body align="center">
	<h3>Add Pet</h3>
	<form action="AddPetServlet" method="post">
		<table border="1">
			<tr>
				<td>Petname</td>
				<td><input type="text" name="petname"></td>
			</tr>

			<tr>
				<td>PetAge</td>
				<td><input type="number" name="age"></td>
			</tr>
			<tr>
				<td>PetPlace</td>
				<td><input type="text" name="place"></td>
			<tr>
				<td><input type="submit" value="Add Pet"></td>
				<td><input type="reset" value="Cancel"></td>
			</tr>
		</table>
	</form>
</body>
</html>