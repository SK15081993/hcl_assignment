<%@page import="com.pet.model.Pet"%>
<%@page import="com.pet.dao.PetDaoImpl"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pet List</title>
</head>
<body align="center">
<a href="home.jsp">Home</a>
	<br />
	<a href="mypet.jsp">MyPet</a>
	<br />
	<a href="addpet.jsp">AddPet</a>
	<br />
	<a href="registration.jsp">Logout</a>
	<br/>
	<h1>List of Pet</h1>
	<table border ="1" width="500" align="center">
		<tr bgcolor="00FF7F">
		<th><b>Pet Id</b></th>
		<th><b>Pet Name</b></th>
		<th><b>Pet Age</b></th>
		<th><b>Pet Place</b></th>
		<th><b>Buy</b></th>
		</tr>
		<tr>
		</tr>
		<%-- Fetching the attributes of the request object
			which was previously set by the servlet
			"StudentServlet.java"
		--%>
		<%
		PetDaoImpl petdao=new PetDaoImpl();
		ArrayList<Pet> plist=petdao.getAllPet();
		
		for(Pet p:plist){%>
		<%-- Arranging data in tabular form
		--%>
			<tr>
			    <td><%=p.getPetid()%></td>
				<td><%=p.getPetname()%></td>
				<td><%=p.getPetage()%></td>
				<td><%=p.getPetplace()%></td>
				<td><form action="BuyPetServlet" method="post"><a href="mypet.jsp">Buy</a></form></td>
			</tr>
			<%}%>			
		</table>
		<hr/>
	</body>
</html>
