package com.Assessment4;

public class OOPExercises { 
	public static void main(String[] args) { 
		A objA = new A();
		System.out.println("in main(): "); 
		System.out.println("objA.a = "+objA.a); objA.a = 222;
		
	} }
class A {
public int a = 100;
//private int a=100;
public void setA( int value) {
a = value;
}
public int getA() {
return a;
}
}
//Because of private variable ,we can not access the variable outside the package..