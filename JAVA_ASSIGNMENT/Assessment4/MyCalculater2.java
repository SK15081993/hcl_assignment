package com.Assessment4;

import java.util.Scanner;

class MyCalculater2 implements AdvancedArithmetic {
	 
	public int divisor_sum(int n) {
	 
	 int sum=0;
	 for(int i=1;i<=n;i++) {
		 if(n%i==0) {
			 sum=sum+i;
		 }
	 }
	 return sum;
 }
	public static void main(String[] args) {
		MyCalculater2 mc=new MyCalculater2();
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number:");
		int num=sc.nextInt();
		System.out.println(mc.divisor_sum(num));
		sc.close();
	}
}