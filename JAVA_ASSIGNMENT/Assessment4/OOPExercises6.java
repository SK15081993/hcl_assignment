package com.Assessment4;

public class OOPExercises6 {
	static int a = 555; 
	public static void main(String[] args) { 
		AA objA = new AA();
		B objB1 = new B(); 
		AA objB2 = new B(); 
		C objC1 = new C(); 
		C objC2 = new C();
		AA objC3 = new C(); 
		objA.display();
		objB1.display(); 
		objB2.display(); 
		objC1.display(); 
		objC2.display();
		objC3.display(); 
	}
}
class AA{
  int a = 100;
   public void display() {
    System.out.printf("a in A = %d\n", a);
}
   }
 
 class B extends AA{
	 int b=200;
	 public void display() {
		 System.out.printf("b in B = %d\n", b);
	 }
	 
 }
 class C extends AA{
	 int c=300;
	 
	 public void display() {
		 System.out.printf("c in C = %d\n", c);
	 }
 }
