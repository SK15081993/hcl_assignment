package com.Assessment4;

import java.util.Scanner;

abstract class Shape{
	 
	protected String name;
	
	public Shape(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	abstract Float calculateArea();
}
 
class Circle extends Shape{

	private Integer radius;
	
	public Circle(String name, Integer radius) {
		super(name);
		this.radius = radius;
	}
	public Integer getRadius() {
		return radius;
	}


	public void setRadius(Integer radius) {
		this.radius = radius;
	}


	Float calculateArea() {
		Float area=Float.valueOf(radius*radius*3.14f);
		return area;
	}
	
}
class Square extends Shape{
	
	private Integer side;
	public Square(String name, Integer side) {
		super(name);
		this.side = side;
	}
	public Integer getSide() {
		return side;
	}


	public void setSide(Integer side) {
		this.side = side;
	}
	Float calculateArea() {
		Float area=Float.valueOf((float)side*side);
		return area;
	}
}

class Rectangle extends Shape{
      private Integer length;
      private Integer width;
      
	public Rectangle(String name, Integer length, Integer width) {
		super(name);
		this.length = length;
		this.width = width;
	}
	public Integer getLength() {
		return length;
	}


	public void setLength(Integer length) {
		this.length = length;
	}


	public Integer getWidth() {
		return width;
	}


	public void setWidth(Integer width) {
		this.width = width;
	}
	Float calculateArea() {
		Float area=Float.valueOf((float)length*width);
		return area;
	}
	
}
public class AbstractShape1 {
	public static void main(String[] args) {

     Scanner sc=new Scanner(System.in);
     System.out.println("Enter the shape name:");
     System.out.println(".Circle\n.Square\n.Rectangle");
     
     String choice=sc.next();
	if(choice.equals("Circle")) {
		System.out.println("Enter the radius:");
		int radius=sc.nextInt();
		Circle cir=new Circle("cir",radius);
		System.out.println("Area of circle is:"+cir.calculateArea());
	}
	else if(choice.equals("Square")) {
		System.out.println("Enter the side:");
		int side=sc.nextInt();
		Square sq=new Square("sq",side);
		System.out.println("Area of square is:"+sq.calculateArea());
	}

	else if(choice.equals("Rectangle")){
		System.out.println("Enter the length:");
		int length=sc.nextInt();
		System.out.println("Enter the width:");
		int width=sc.nextInt();
		Rectangle rg=new Rectangle("rg",length,width);
		System.out.println("Area of rectangle:"+rg.calculateArea());	
	}
	sc.close();
	}

}
