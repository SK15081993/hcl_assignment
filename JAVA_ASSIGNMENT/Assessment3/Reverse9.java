package com.Assessment3;

import java.util.Scanner;

 class UserMainCode9 {
	
	public static String stringModify(char ch,String str) {
		String mod="";
		char[] char1=str.toCharArray();
		for(int i=char1.length-1;i>=0;i--) {
			if(i==0) {
				mod=mod+char1[0];
			}
			else {
				mod=mod+char1[i]+Character.toString(ch);
			}
		}
		return mod;
	}

}
class Reverse9{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String:");
		String str=sc.nextLine();
		System.out.println("Enter the character:");
		char ch=sc.next().charAt(0);
		System.out.println("The Modified String is:"+UserMainCode9.stringModify(ch, str));
		sc.close();
		
	}
}
