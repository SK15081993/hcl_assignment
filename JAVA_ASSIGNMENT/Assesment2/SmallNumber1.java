package com.Assesment2;

import java.util.Scanner;

class Number{
	
	public static void smallNumber(int x,int y,int z) {
		
		 if(x<=y && x<=z){
		        System.out.println(x+" is the smallest number");
		        
		    }
		    else if(y<=x && y<=z){
		        System.out.println(y+" is the smallest number");
		        
		    }
		    else{
		        System.out.println(z+" is the smallest number");
		        
		    }
		    
		}
	}
	
public class SmallNumber1 {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter first number:");
		int x=sc.nextInt();
		
		System.out.println("Enter second number:");
		int y=sc.nextInt();
		
		System.out.println("Enter third number:");
		int z=sc.nextInt();
		
		Number.smallNumber(x,y,z);
		

		
	}

}
