package com.servlet.assessment;

/*2) Write a servlet application to establish communication between html and servlet page using hyperlink.
Ref : <a href = "http://localhost:8080/ServletApp/anchortag">check</a>
*/
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HyperLinkServlet
 */
@WebServlet("/HyperLinkServlet")
public class HyperLinkServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
	     	response.setContentType("text/html");//set response content type
            PrintWriter out=response.getWriter(); //get printWrite object
              out.println("WELCOME TO HCL");
              out.println("<a href='hyperlink.html'>Home</a>");
    }

}
