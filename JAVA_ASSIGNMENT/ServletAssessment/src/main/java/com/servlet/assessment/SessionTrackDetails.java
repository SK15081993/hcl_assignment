package com.servlet.assessment;

/*6) Write an application to demonstrate the session tracking in Servlet with following details
*/
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/SessionTrackDetails")
public class SessionTrackDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private int visitsCounter;
	public void init(ServletConfig config) {
	}
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		HttpSession session=request.getSession(true);
		String username=request.getParameter("username");
		session.setAttribute("username",username);
		String htmlcode="<!DOCTYPE html>\r\n" + 
				"<html>\n" + 
				"<head>\n" + 
				"<title>My Website</title>\r\n" + 
				"<style>\r\n" + 
				"div{\r\n" + 
				"background-color: rgb(170,226,40);\r\n" + 
				"width: 400px;\r\n" + 
				"}\r\n" + 
				"</style></head>\r\n" + 
				"<body>\r\n" + 
				"<div><h2 align=\"center\">Welcome Back To My Website</h2>\r\n" + 
				"<h4 align=\"center\">Session Information</h4><table border=''>\r\n" + 
				"<tr>\r\n" + 
				"<th bgcolor='rgb(197,199,35)'>Session info</th><th bgcolor='rgb(197,199,35)' >Value</th></tr>\r\n" + 
				"<tr>\r\n" + 
				"<td>ID</td><td>"+session.getId()+"</td>\r\n" + 
				"</tr>\r\n" + 
				"<tr>\r\n" + 
				"<td>Creation time</td><td>"+session.getCreationTime()+"</td>\r\n" + 
				"</tr>\r\n" + 
				"<tr>\r\n" + 
				"<td>Time Of Last Access</td><td>"+session.getLastAccessedTime()+"</td>\r\n" + 
				"</tr>\r\n" + 
				"<tr>\r\n" + 
				"<td>User ID</td><td>"+session.getAttribute("username").toString()+"</td>\r\n" + 
				"</tr>\r\n" + 
				"<tr>\r\n" + 
				"<td>Number of visits</td><td>"+(++visitsCounter)+"</td>\r\n" + 
				"</tr>\r\n" + 
				"</table>\r\n" + 
				"</div></body>\r\n" + 
				"</html>";
		response.getWriter().println(htmlcode);
		
	}

}

