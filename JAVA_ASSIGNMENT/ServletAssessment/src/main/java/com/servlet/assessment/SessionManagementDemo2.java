package com.servlet.assessment;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/SessionManagementDemo2")
public class SessionManagementDemo2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();
		HttpSession session=request.getSession();
		//session.invalidate();//4/06/21 adding today destroying my session in this page
		//session.setMaxInactiveInterval(1800);//30min
		out.println("<a href='SessionManagementDemo3'>Payment<br/></a>");
		out.println("Hello"+session.getAttribute("username"));
		//session.invalidate();//demo3 session getdestroyed
		//session.setMaxInactiveInterval(1800);
	}
}
