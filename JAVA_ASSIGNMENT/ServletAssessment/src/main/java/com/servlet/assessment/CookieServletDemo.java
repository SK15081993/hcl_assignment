package com.servlet.assessment;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/CookieServletDemo")
public class CookieServletDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		String username=request.getParameter("username");
		out.println("welcome "+username);//value come from text box of html
		//Cookie cookie=new Cookie("username",username);
		
		
		  String cookieencode=URLEncoder.encode(username,"UTF-8");//adding just to remove  space problem in text boc 
		 Cookie cookie=new Cookie("username",cookieencode);
		 
		  response.addCookie(cookie);
		  out.println("<form action='CookieServetlDemo2'>");
		  out.println("<input type='submit' value='submit'>");
		  out.println("</form>");
		
	}

}
