package com.servlet.assessment;

/*3) Write a servlet that refreshes the browser every second and current time will be changed automatically.
Ref : GregorianCalendar , response header*/

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/RefreshServlet")
public class RefreshServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		   response.setContentType("text/html");
		   response.setHeader("refresh","3");
		   Calendar calendar=new GregorianCalendar();
		   int hour=calendar.get(Calendar.HOUR);
		   int minute=calendar.get(Calendar.MINUTE);
		   int second=calendar.get(Calendar.SECOND);
		   String ct=hour+"::"+minute+"::"+second;
		   PrintWriter out=response.getWriter();
		   String title="Browser refresh after three second";
		   out.println("<body bgcolor='green'>"+"<h1 align='center'>"+title+"</h1>"+"<p>Current time is:"+ct+"</p>");     
		       	    
		}

}
