package com.servlet.assessment;

/*4) Write a Servlet application to count the total number of visits on the website.
*/

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CountVisitServlet")
public class CountVisitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	int counter=0;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
        out.println("<form>");
        out.println("<h3>Welcome to HCL!</h>");
        out.println("Visitor number of:"+(++counter));
        out.println("</form>");
		}
}

