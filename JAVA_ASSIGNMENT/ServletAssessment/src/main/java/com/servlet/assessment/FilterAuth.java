package com.servlet.assessment;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * Servlet Filter implementation class FilterAuth
 */
@WebFilter(urlPatterns="/ServletAuth",servletNames = { "ServletAuth" })
public class FilterAuth implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		PrintWriter out=response.getWriter();
		RequestDispatcher rd=request.getRequestDispatcher("filter.jsp");
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		response.setContentType("text/html");
		if (username.equals("simi") && password.equals("simi")) {
			chain.doFilter(request, response);
		}
		else if(username.equals("") || password.equals("")) {
			out.println("please enter both username and password");
			rd.include(request, response);
		}
		else if(!username.equals("simi") || !password.equals("simi")) {
			out.println("wrong username or password");
			rd.include(request, response);
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}