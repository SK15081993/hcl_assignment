package com.servlet.assessment;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.databaseconnection.DBConnection;

/**
 * Servlet implementation class LoginRegister
 */
@WebServlet("/LoginRegister")
public class LoginRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Connection con=null;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username=request.getParameter("username");
		PrintWriter out=response.getWriter();
		String password=request.getParameter("password");
		
		try {
			 try {
				con=DBConnection.getConnection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		Statement st=con.createStatement();
		int status=st.executeUpdate("insert into  logindetails values('"+username+"','"+password+"')");
		if (status==1){
			out.println("Registered successfully");
			response.setContentType("text/html");
			out.println("<a href='WebLogout.jsp'>Exit</a>");
			out.println("<a href='WebLogin.jsp'>Login</a>");
		}
		else {
			out.println("Could not register");
			request.getRequestDispatcher("WebRegister.jsp").include(request, response);
		}
		}catch(SQLException sqe) {
			sqe.printStackTrace();
		}
	}
	}
