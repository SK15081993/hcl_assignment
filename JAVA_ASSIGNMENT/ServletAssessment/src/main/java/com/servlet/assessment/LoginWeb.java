package com.servlet.assessment;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class oginWeb
 */
@WebServlet("/LoginWeb")
public class LoginWeb extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd=request.getRequestDispatcher("WebLogin.jsp");
		PrintWriter out=response.getWriter();
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		Login login=new Login(username,password);
		boolean status=false;
		Connection con=null;
		try {
		try {
			con=DBConnection.getConnection();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement st=con.createStatement();
	  ResultSet rs= st.executeQuery("select * from logindetails where username='"+login.getUsername()+"' and password='"+login.getPassword()+"'");
		if (rs.next()==false)
			status=false;
		else
			status=true;
		}catch(SQLException sqe) {
			System.out.println(sqe);;}
		if (status==true) {
			HttpSession session=request.getSession();
			session.setAttribute("username", username);
			session.setMaxInactiveInterval(15*60);
			RequestDispatcher rd2=request.getRequestDispatcher("welcomelogged.jsp");
			rd2.forward(request, response);
		}
		else {
			out.println("Incorrect credentials");
			rd.include(request, response);
		}
		out.println("<a href='WebLogout.jsp'>Exit</a><br><a href='welcomeweb.jsp'>Home</a>");
	}

}
