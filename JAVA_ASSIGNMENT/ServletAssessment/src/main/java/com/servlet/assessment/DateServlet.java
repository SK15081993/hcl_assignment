package com.servlet.assessment;

/*1) Write Servlet application to print current date & time?
Ref : use simple Date object*/

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;

@WebServlet("/DateServlet")
public class DateServlet extends GenericServlet {
	private static final long serialVersionUID = 1L;
    
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		 
        response.setContentType("text/html");//set response content type
        PrintWriter out=response.getWriter();//get stream obj
         Date date=new Date(); //create date object
        out.println("<h2>"+"Current Date and Time:"+date+"</h2>");
	}

}
