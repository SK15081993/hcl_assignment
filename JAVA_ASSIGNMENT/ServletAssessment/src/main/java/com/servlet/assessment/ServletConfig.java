package com.servlet.assessment;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ServletConfig")
public class ServletConfig extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();
		String num1=getInitParameter("num1");
		String num2=getInitParameter("num2");
		out.println("Number1 value is:"+num1+"<br/>");
		out.println("Number2 value is:"+num2+"<br/>");
		int sum=Integer.parseInt(num1)+Integer.parseInt(num2);
		out.println("Result is:"+sum);

		
	}

}
