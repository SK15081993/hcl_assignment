package com.servlet.assessment;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/CookieServetlDemo2")
public class CookieServetlDemo2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Cookie cookiearray[]=request.getCookies();
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		//out.println(cookiearray[0].getValue());for single value
		  for(Cookie cookie:cookiearray) {
	
		  //out.println("Hello"+cookie.getValue());//without getvalue we will get relevent object
		  out.println("Hello"+cookie.getValue().replace("+"," "));
	
		  }
		 
	}
}
