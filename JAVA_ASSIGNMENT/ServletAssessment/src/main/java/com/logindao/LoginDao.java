package com.logindao;

import com.entity.LoginDetails;

public interface LoginDao {
	
	public boolean checkLoginInfo(LoginDetails logindetails) throws Throwable;

}
