package com.registration;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private ServletRequest request;
	String username=request.getParameter("name");
	String password=request.getParameter("password");
	String email=request.getParameter("email");
	String mobile=request.getParameter("mobile");
	String gender=request.getParameter("gender");
	String country=request.getParameter("country");
	String dob=request.getParameter("dob");
	
	Registration register=new Registration(username,password,email,mobile,dob,gender,country);
	RegisterServiceImpl rsi=new RegisterServiceImpl();
	rsi.addAccount(register);
}

