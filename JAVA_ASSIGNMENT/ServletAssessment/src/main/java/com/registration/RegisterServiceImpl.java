package com.registration;

public class RegisterServiceImpl {
	public Registration displayAccounts(Registration register) throws Throwable{
		return new RegisterDaoImpl().displayAccounts(register);
	}
	public boolean addAccount(Registration register) throws Throwable {
		return new RegisterDaoImpl().addAccount(register);
	}
}
