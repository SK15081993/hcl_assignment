package com.registration;

public interface RegisterService {
	public Registration displayAccounts(Registration register);
	public boolean addAccount(Registration register);
}
