package com.registration;

public class Registration {

	private int registerid;
	private String first_name;
	private String email;
	private String mobile;
	private String dob;
	private String password;
	private String gender;
	private String country;
	
	public Registration() {}
	public Registration(String first_name,String password,String email,String mobile,String dob,String gender,String country) {
		this.first_name=first_name;
		this.email=email;
		this.mobile=mobile;
		this.dob=dob;
		this.gender=gender;
		this.country=country;
		this.password=password;
	}
	public Registration(String first_name,String email,String mobile,String dob,String gender,String country) {
		this.first_name=first_name;
		this.country=country;
		this.dob=dob;
		this.gender=gender;
		this.email=email;
		this.mobile=mobile;
	}
	public Registration(String first_name,String password) {
		this.first_name=first_name;
		this.password=password;
	}
	public int getRegisterid() {
		return registerid;
	}
	public void setRegisterid(int registerid) {
		this.registerid = registerid;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	@Override
	public String toString() {
		return "Registration [registerid=" + registerid + ", first_name=" + first_name + ", email=" + email
				+ ", mobile=" + mobile + ", dob=" + dob + ", password=" + password + ", gender=" + gender + ", country="
				+ country + "]";
	}
	
	
		}

