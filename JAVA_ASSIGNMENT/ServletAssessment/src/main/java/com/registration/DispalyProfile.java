package com.registration;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/DispalyProfile")
public class DispalyProfile extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		String name=request.getParameter("username");
		String password=request.getParameter("password");
		Registration register=new Registration(name,password);
		RegisterServiceImpl rs=new RegisterServiceImpl();
		Registration registerDisplay;
		try {
			registerDisplay = rs.displayAccounts(register);
			response.setContentType("text/html");

			out.println("<table border='1'><tr><th>Name</th><th>Email</th><th>Mobile</th><th>Date</th><th>Gender</th><th>Country</th></tr><tr><td>"+registerDisplay.getFirst_name()+"</td><td>"+registerDisplay.getEmail()+"</td><td>"+registerDisplay.getMobile()+"</td><td>"+registerDisplay.getDob()+"</td><td>"+registerDisplay.getGender()+"</td><td>"+registerDisplay.getCountry()+"</td></tr></table>");

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}