package com.registration;

public interface RegisterDao {

	public Registration displayAccounts(Registration register) throws Exception, Throwable;
	public boolean addAccount(Registration register) throws Exception, Throwable;

}
