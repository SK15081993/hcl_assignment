package com.mainservlet;


/*7) Write a Servlet application for login page, which is check the username and password. If username and password are matched, display welcome message along with username
Ref : login.html and LoginServlet . username and password can be hard coded or retrieved from database through service and dao layer?
*/
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.entity.LoginDetails;
import com.service.LoginServiceImpl;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		
		LoginServiceImpl login_Service=new LoginServiceImpl();
		
		LoginDetails ld=new LoginDetails(username,password);
		try {
			boolean status=login_Service.checkLoginInfo(ld);
			if(status==true) {
				out.println("Welcome"+username);
			}else {
				out.println("username or password is invalid");
				RequestDispatcher rd=request.getRequestDispatcher("login.html");
				rd.include(request,response);
			}
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
