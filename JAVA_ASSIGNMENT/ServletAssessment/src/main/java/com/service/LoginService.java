package com.service;

import com.entity.LoginDetails;

public interface LoginService {

	public boolean checkLoginInfo(LoginDetails logindetails) throws Throwable;
		
	}

