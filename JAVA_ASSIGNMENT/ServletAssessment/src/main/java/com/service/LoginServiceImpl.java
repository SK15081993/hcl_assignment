package com.service;

import com.entity.LoginDetails;
import com.logindao.LoginDaoImpl;

public class LoginServiceImpl implements LoginService {

	public boolean checkLoginInfo(LoginDetails logindetails) throws Throwable {
		LoginDaoImpl logindao=new LoginDaoImpl();
		boolean status=logindao.checkLoginInfo(logindetails);
		return status;
		
	}
}
