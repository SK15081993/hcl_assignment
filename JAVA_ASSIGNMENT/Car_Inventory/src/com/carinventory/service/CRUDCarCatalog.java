package com.carinventory.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.carinventary.entities.CarEntity;
import com.carinventory.db.DBConnection;

public class CRUDCarCatalog {
	
	    DBConnection con=null;
	    Connection connection=null;
	    PreparedStatement ps=null;
	    Statement st=null;
	    ResultSet rs=null;
	  
	  public void addCar(CarEntity car) throws Exception {
		 
		connection=con.getConnection();
		
		st=connection.createStatement();
		
		String make=car.getMake();
		String model=car.getModel();
		int year=car.getYear();
		float sale_price=car.getSale_price();
		
		  rs = st.executeQuery("select count(model) from car");
		  int count=0;
		while(rs.next()) {
			
		  count=rs.getInt(1);
		}
		/* System.out.println(count); */
		if(count<20) {
			int noOfRows=st.executeUpdate("insert into car(make,model,year,sale_price) values('"+make+"','"+model+"',"+year+","+sale_price+")");
	         System.out.println("Car inserted in catalog successfully"+"\n");
	         
		}
		else {
			System.out.println("Car data can't be inserted,you have reached max. limit"+"\n");
			
		}
	  }
	  
	  public void listCar() throws Exception {
		  Connection connection = con.getConnection();
		    st=connection.createStatement();
		    String sql = "SELECT *from car";

		     rs=st.executeQuery(sql);
		     while (rs.next()) {
		    	   System.out.println(rs.getString("make") + "\t" + 
		    			               rs.getString("model") + "\t" + 
		    	                      rs.getInt("year")  + "\t" +
		    	                      rs.getFloat("sale_price"));     
		    	}
		  System.out.println();
	  }
	
	  public void deleteCar(String model) throws Exception {
		  Connection connection = con.getConnection();
		  
		  String query = "delete from car where model = ?";
		   ps=connection.prepareStatement(query);
		   
		   ps.setString(1,model);
		   ps.execute();
		   System.out.println("Car delected from catalog successfully"+"\n");
	  }
	  
	  public void updateCar(CarEntity car,String model) throws Exception {
      Connection connection = con.getConnection();
		  
      String query = "update car set make = ?,model=?,year=?,sale_price=? where model = ?";
                ps = connection.prepareStatement(query);
           
                ps.setString(1,car.getMake());
                ps.setString(2,car.getModel());
                ps.setInt(3,car.getYear());
                ps.setFloat(4,car.getSale_price());
                ps.setString(5,model);
                
               int result=ps.executeUpdate();
           System.out.println(result+"car updated in catalog successfully"+"\n");
           
	  }
	  
}
