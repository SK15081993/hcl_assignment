package com.carinventory.controler;

import java.util.Scanner;

import com.carinventary.entities.CarEntity;
import com.carinventory.service.CRUDCarCatalog;

public class CarMain {
	public static void main(String[] args) throws Exception {
	
		CRUDCarCatalog ad=new CRUDCarCatalog();
		
		int count=0;
		while(count!=1) {
		System.out.println("***Welcome to Singh Joe's Gently Used Autos!***"+"\n");
        System.out.println("1.Add a car to the catalog.");
        System.out.println("2.Dist all cars in the catalog.");
        System.out.println("3.Delete the car using model number.");
        System.out.println("4.Update car details using model number.");
        System.out.println("5.Quit the program.");
        System.out.println("Enter your choice:");
        
        Scanner sc=new Scanner(System.in);
        String choice=sc.next();
      
            if(choice.equals("1")) {
        	CarEntity car= new CarEntity();
        	System.out.println("You opted for adding a car to the catalog.");
            System.out.println("Enter Car making company name:");
            String make=sc.next();
            car.setMake(make);
            System.out.println("Enter model number:");
            String model=sc.next();
            car.setModel(model);
            System.out.println("Enter making of Year:");
            int year=sc.nextInt();
            car.setYear(year);
            System.out.println("Enter sale price:");
            float price=sc.nextFloat();
            car.setSale_price(price);
              try {
				ad.addCar(car);
			} catch (Exception e) {
		
				e.printStackTrace();
			}
             
     }
        else if(choice.equals("2")) {
        	System.out.println("You opted for listing car in the catalog.");
            
            try {
            	ad.listCar();
            	
            }catch(Exception e) {
            	e.printStackTrace();
            }
           
        }else if(choice.equals("3")){
        
            System.out.println("Delete the car using model number:");
            System.out.println("Enter model number:");
            String model=sc.next();
            ad.deleteCar(model);
            
        }else if(choice.equals("4")) {
            CarEntity car=new CarEntity();
            System.out.println("Update the car using model number:");
            System.out.println("Enter model number:");
            String model=sc.next();
            
            
            System.out.println("Enter Car making company name:");
            String make=sc.next();
            car.setMake(make);
            System.out.println("Enter model number:");
            String model1=sc.next();
            car.setModel(model1);
            System.out.println("Enter making of Year:");
            int year=sc.nextInt();
            car.setYear(year);
            System.out.println("Enter sale price:");
            float price=sc.nextFloat();
            car.setSale_price(price);
            ad.updateCar(car,model);
           
        }else if(choice.equals("5")){
        	count=1;
        	System.out.println("Thank You!");
        	
        }
        else {
        	System.out.println("Sorry, but "+choice+" is not a valid command.Please try again."+"\n");
        }
        }
        
	}}
