package com.assessment7;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/*10.Write a program to read  two String variables in DD-MM-YYYY.Compare the two dates and return the older date in 'MM/DD/YYYY' format. 
Include a class UserMainCode with a static method findOldDate which accepts the string values. The return type is the string. 
Create a Class Main which would be used to accept the two string values and call the static method present in UserMainCode. 
*/
 class UserMainCode2{
	 
	public static String findOldDate(String d1,String d2) throws ParseException{
		
		SimpleDateFormat sdf=new SimpleDateFormat("DD-MM-YYYY");
		Date date1=sdf.parse(d1);
		Date date2=sdf.parse(d2);
		
		SimpleDateFormat sdfnew=new SimpleDateFormat("DD/MM/YYYY");
		if (date1.before(date2)) {
			return sdfnew.format(date1);
		}
		else {
			return sdfnew.format(date2);
		}	
	}
 }
public class Main1{
	
	public static void main(String args[]) throws ParseException {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the first date");
		String d1=sc.next();
		System.out.println("Enter the second date");
		String d2=sc.next();
		System.out.println("Olde date is:");
		System.out.println(UserMainCode2.findOldDate(d1,d2));
	}
}