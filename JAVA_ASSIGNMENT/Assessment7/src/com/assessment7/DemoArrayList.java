package com.assessment7;

/*8.Write a code to read two int array lists of size 5 each as input and to merge the two arrayLists, 
sort the merged arraylist in ascending order and fetch the elements at 2nd, 6th and 8th index into a new arrayList and 
return the final ArrayList.  Include a class UserMainCode with a static method sortMergedArrayList which accepts 2 ArrayLists. 
The return type is an ArrayList with elements from 2,6 and 8th index position .Array index starts from position 0. 
Create a Main class which gets two array list of size 5 as input and call the static method sortMergedArrayList present in the UserMainCode. 
*/


import java.util.*;

 public class DemoArrayList {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		List<Integer> list=new ArrayList<Integer>(5);
		System.out.println("Enter five integer for the first list:");
		for (int i=0;i<5;i++) {
			int val=sc.nextInt();
			list.add(val);
		}
		List<Integer> list1=new ArrayList<Integer>(5);
		System.out.println("Enter 2'nd five integer for the second list:");
		for (int i=0;i<5;i++) {
		int val=sc.nextInt();
		list1.add(val);
		}
		System.out.println("The values at 2nd,6th and 8th positions of the merged sorted list:");
		List<Integer> final_ArrayList=UserMainCodeDemo.sortMergedArrayList(list,list1);
		System.out.println(final_ArrayList.get(0));
		System.out.println(final_ArrayList.get(1));
		System.out.println(final_ArrayList.get(2));
		sc.close();
	}

}
 class UserMainCodeDemo {
	public static List<Integer> sortMergedArrayList(List<Integer>list,List<Integer>list1){
		List<Integer> mergedArray_List=new ArrayList<Integer>();
		mergedArray_List.addAll(list);
		mergedArray_List.addAll(list1);
		Collections.sort(mergedArray_List);
		List<Integer> final_ArrayList=new ArrayList<Integer>(3);
		final_ArrayList.add(mergedArray_List.get(2));
		final_ArrayList.add(mergedArray_List.get(6));
		final_ArrayList.add(mergedArray_List.get(8));
		return final_ArrayList;
		
	}
}