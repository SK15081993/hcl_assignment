package com.assessment7;



/*14.Given a date string in the format dd/mm/yyyy, write a program to convert the given date to the format dd-mm-yy. 
Include a class UserMainCode with a static method “convertDateFormat” that accepts a String and returns a String. 
 Create a class Main which would get a String as input and call the static method convertDateFormat present in the UserMainCode. 
Sample Input: 
12/11/1998 
  
Sample Output: 
12-11-98 
*/
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

class DateConversionUserMainCode{
	public static String convertDateFormat(String oldDate) throws ParseException {
	     SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
	     Date oldParsedDate=sdf.parse(oldDate);
	     SimpleDateFormat sdfnew=new SimpleDateFormat("dd-MM-yy");
	     String newParsedDate=sdfnew.format(oldParsedDate).toString();
	     return newParsedDate;	
	}
}
class DateConversion {
	public static void main(String[] args) throws ParseException {
		Scanner sc=new Scanner(System.in);
		String oldDate=sc.next();
		System.out.println("The date in changed format is::"+DateConversionUserMainCode.convertDateFormat(oldDate));
	}

}
