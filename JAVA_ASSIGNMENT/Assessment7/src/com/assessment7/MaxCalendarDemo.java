package com.assessment7;

/*4.Write a Java program to get the maximum value of the year, month, week, 
date from the current date of a default calendar.*/


import java.util.*;

public class MaxCalendarDemo{
   public static void main(String[] args) {
    
        Calendar cal = Calendar.getInstance();
		System.out.println("Maximum value of the year, month, week,date:");
		System.out.println("Current Date and Time:"+cal.getTime());
		int maxWeek = cal.getActualMaximum(Calendar.WEEK_OF_YEAR);
		int maxDate = cal.getActualMaximum(Calendar.DATE);
		int maxMonth = cal.getActualMaximum(Calendar.MONTH);
		int maxYear = cal.getActualMaximum(Calendar.YEAR);
		
		System.out.println("Maximum Week of Year:"+maxWeek);
		System.out.println("Maximum Date:"+maxDate);
		System.out.println("Maximum Month:"+maxMonth);
		System.out.println("Maximum Year:"+maxYear);
	  }
}
