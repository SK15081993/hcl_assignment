package com.assessment7;

/*5.Given two inputs year and month (Month is coded as: Jan=0, Feb=1 ,Mar=2 ...)
write a program to find out total number of days in the given month for the given year.*/ 

import java.util.*;

public class CountMonth{
	
  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int year=sc.nextInt();
    int month=sc.nextInt();
    System.out.println(display(year,month));
  }
  public static int display(int year,int month) {
    Calendar cal=Calendar.getInstance();
    cal.set(Calendar.YEAR, year);
    cal.set(Calendar.MONTH, month);
    int day_of_month=cal.getActualMaximum(cal.DAY_OF_MONTH);
    return day_of_month;
  }
}