package com.assessment7;

/*9. A Company wants to obtain employees of a particular designation. You have been assigned as the programmer to build this package. You would like to showcase your skills by creating a quick prototype. The prototype consists of the following steps: 
    Read Employee details from the User. The details would include name and designaton in the given order. The datatype for name and designation is string. 
    Build a hashmap which contains the name as key and designation as value. 
    You decide to write a function obtainDesignation which takes the hashmap and designation as input and returns a string array of employee names who belong to that designation as output. Include this function in class UserMainCode. 
Create a Class Main which would be used to read employee details in step 1 and build the hashmap. Call the static method present in UserMainCode. 
*/
import java.util.*;
import java.util.Map.Entry;
import java.util.Scanner;

class UserMainCode1 {
	
	public static List<String> obtainDesignation(HashMap<String,String>map,String designation){
		
		List<String> employeeName=new ArrayList<String>();
		for(Entry<String,String> entry:map.entrySet()) {
			if (entry.getValue().equals(designation))
				employeeName.add(entry.getKey());
		}
		return employeeName;
	}
}
public class Main {
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of employee:");
		int size=sc.nextInt();
		sc.nextLine();
		HashMap<String,String> map=new LinkedHashMap<String,String>();
		
		for (int i=1;i<=size;i++) {
			
			System.out.println("Enter the name of "+i+" employee:");
			String name=sc.nextLine();
			System.out.println("Enter the designation of "+i+" employee:");
			String designation=sc.nextLine();
			map.put(name,designation);
		}
		
		System.out.println("Enter the designation of employee for that name which you want:");
		String searchDesig=sc.nextLine();
		List<String> employeeName=UserMainCode1.obtainDesignation(map,searchDesig);
		Iterator<String> itr=employeeName.iterator();
		while (itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}

}
