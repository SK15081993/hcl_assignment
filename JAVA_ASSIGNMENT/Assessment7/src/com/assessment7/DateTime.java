package com.assessment7;


/*1.Write a java program to print current date and time in the specified format. 
*/
import java.time.format.*;  
import java.time.*;  

public class DateTime {    
  public static void main(String[] args) {    
   DateTimeFormatter dtf=DateTimeFormatter.ofPattern("YYYY/MM/dd HH:mm:ss");  
   LocalDateTime now=LocalDateTime.now();  
   System.out.println(dtf.format(now));  
  }    
}

