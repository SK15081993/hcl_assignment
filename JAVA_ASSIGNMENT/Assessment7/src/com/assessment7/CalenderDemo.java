package com.assessment7;

/*3.Write a Java program to get and display information (year, month, day, hour, minute) of a default calendar.
*/
import java.util.*;


public class CalenderDemo {
 public static void main(String[] args) {
 
        Calendar cal = Calendar.getInstance();
	    System.out.println("Display current date from the Default Calendar::");
	    System.out.println("Day:"+cal.get(Calendar.DATE));
	    System.out.println("Month:" +cal.get(Calendar.MONTH));
        System.out.println("Year:" +cal.get(Calendar.YEAR));
        System.out.println("Hour:"+cal.get(Calendar.HOUR));
        System.out.println("Minute:"+cal.get(Calendar.MINUTE));
        System.out.println("Second:"+cal.get(Calendar.SECOND));
    }
}