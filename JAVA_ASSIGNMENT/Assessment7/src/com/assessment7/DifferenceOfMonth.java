package com.assessment7;

/*11.Given a method with two date strings in yyyy-mm-dd format as input.
 Write code to find the difference between two dates in months.  
Include a class UserMainCode with a static method getMonthDifference which accepts two date strings as input.  
The return type of the output is an integer which returns the diffenece between two dates in months.  
Create a class Main which would get the input and call the static method getMonthDifference present in the UserMainCode. 
*/
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


class UserMainCode3{
	public static int getMonthDifference(String d1,String d2) throws ParseException {
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-mm-dd");
		Date date1=sdf.parse(d1);
		Date date2=sdf.parse(d2);
		
		int monthdiff=(int) (date1.getTime()-date2.getTime());
		return monthdiff;
	}
}
public class DifferenceOfMonth {
	public static void main(String args[]) throws ParseException {
		
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter first date:");
		String d1=sc.next();
		System.out.println("Enter second date:");
		String d2=sc.next();
		System.out.println(UserMainCode3.getMonthDifference(d1,d2));
		
	}
	

}
