package com.assessment7;


/*12.Write a program to read a string and validate the IP address. Print “Valid” if the IP address is valid, else print “Invalid”. 
  
Include a class UserMainCode with a static method ipValidator which accepts a string. The return type (integer) should return 1 if it is a valid IP address else return 2. 
Create a Class Main which would be used to accept Input String and call the static method present in UserMainCode. 
 
Note: An IP address has the format a.b.c.d where a,b,c,d are numbers between 0-255. 
  */

import java.util.Scanner;

public class IpAddress{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String ipAddress=sc.next();
		int status=UserMainIpAddresss.ipValidator(ipAddress);
		if (status==1) {
			System.out.println("Valid");
		}
		else if(status==2) {
			System.out.println("Invalid");
		}
		sc.close();
	}

}


  class UserMainIpAddresss {
	public static int ipValidator(String ipAddress) {
	String[] ipParts=ipAddress.split("\\.");
	int part1=Integer.parseInt(ipParts[0]);
	int part2=Integer.parseInt(ipParts[1]);
	int part3=Integer.parseInt(ipParts[2]);
	int part4=Integer.parseInt(ipParts[3]);
	if ((part1>=0 && part1<=255) && (part2>=0 && part2<=255) && (part3>=0 && part3<=255) && (part4>=0 && part4<=255)) {
		return 1;
	}
	else {
		return 2;
	}
	}
}