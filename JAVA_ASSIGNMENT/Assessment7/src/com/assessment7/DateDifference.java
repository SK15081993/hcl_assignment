package com.assessment7;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

class DateDiffUserMainCode {
	  
    public static int
    getDateDifference(String sdate,String edate) throws ParseException{
  
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            Date d1 = sdf.parse(sdate);
            Date d2 = sdf.parse(edate);

            long difference_In_Time=d2.getTime() - d1.getTime();
            int difference_In_Days=(int)(difference_In_Time/(1000 * 60 * 60 * 24))% 365;
            System.out.println(difference_In_Days);
            return difference_In_Days;
        }
    }

 class DateDifference{
    public static void main(String[] args) throws ParseException
    {
    	Scanner sc=new Scanner(System.in);
    	System.out.println("Enter the start date:");
        String sdate=sc.next();
        System.out.println("Enter the end date:");
        String edate=sc.next();
        System.out.println(DateDiffUserMainCode.getDateDifference(sdate,edate));
    }
}
