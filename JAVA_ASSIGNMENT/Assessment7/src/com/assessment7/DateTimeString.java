package com.assessment7;

/*2.Write a Java program to extract date, time from the date string.
 */ 
import java.util.*;
import java.text.*;

public class DateTimeString {
	  public static void main(String[] args) throws Throwable
	   {
	      String str = "2021-05-21 09:00:02";
	      Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
	      String newstr = new SimpleDateFormat("MM/dd/yyyy,H:mm:ss").format(date);
	       System.out.println("\n"+newstr+"\n");
	     
	    }
	}