package com.assessment6;

/*3.Write a program to create a class DemoThread1 implementing Runnable interface.
In the constructor, create a new thread and start the thread. In run() display a message "running child Thread in loop : "
display the value of the counter ranging from 1 to 10. Within the loop put the thread to sleep for 2 seconds.
In main create 3 objects of the DemoTread1 and execute the program. 
*/

class DemoThread implements Runnable {
	
	DemoThread(){
		Thread t=new Thread();
		t.start();
	}
	public void run() 
	{		
		System.out.println("Running child thread in loop:");
		try {
            for(int i=1;i<=10;i++){ 
            	System.out.println(i);
                Thread.sleep(2000);
               }
      }catch(InterruptedException e){ 
    	  e.printStackTrace();
    	  System.out.println(e);
    }
	 }
  	}
  class DemoThread1{
	public static void main(String[] args) {
		DemoThread dt=new DemoThread();
		DemoThread dt1=new DemoThread();
		DemoThread dt2=new DemoThread();
		Thread t1=new Thread(dt);
		Thread t2=new Thread(dt1);
		Thread t3=new Thread(dt2);
		t1.start();
		t2.start();
		t3.start();
	}
		
	}  