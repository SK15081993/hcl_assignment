package com.assessment6;

/*5.Write a program to create a class Number which implements Runnable.
Run method displays the multiples of a number accepted as a parameter. 
In main create three objects - first object should display the multiples of 2,
second should display the multiples of 5 and third should display the multiples of 8. 
Display appropriate message at the beginning and ending of thread. 
The main thread should wait for the first object to complete. Display the status of threads before the multiples are 
displayed and after completing the multiples. */


  class JoinsDemo implements Runnable{
         int number ;
         
        public JoinsDemo(int number) {
        	this.number=number;
		}
      public void run() {
     for(int i=1;i<=10;i++)
     {
    	 System.out.println(number*i);
     }
	
       }
     }
  public class Joins{

    public static void main(String args[]){
  
    	JoinsDemo t1=new JoinsDemo(2);
    	JoinsDemo t2=new JoinsDemo(5);
    	JoinsDemo t3=new JoinsDemo(8);
    	
    	Thread thread1=new Thread(t1);
        Thread thread2=new Thread(t2);
        Thread thread3=new Thread(t3);
        
      System.out.println("Thread-1 started");
               thread1.start();
      try {
    	  thread1.join();
	  } catch (InterruptedException e) {
		e.printStackTrace();
		System.out.println(e);
     	}
      System.out.println("Thread-1 Completed");
      System.out.println("Thread-2 started");
               thread2.start();
      try {
    	  thread2.join();
	   } catch (InterruptedException e) {
		e.printStackTrace();
		System.out.println(e);
        	}
      System.out.println("Thread-2 Completed");
      
      
      System.out.println("Thread-3 started");
                 thread2.start();
      try {
    	  thread3.join();
	    } catch (InterruptedException e) {
		e.printStackTrace();
		System.out.println();
     	}
       System.out.println("Thread-3 Completed");
      
    }
  }

