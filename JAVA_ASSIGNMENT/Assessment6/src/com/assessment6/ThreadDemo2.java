package com.assessment6;

import java.time.LocalDate;

/*2.In the previous program remove the try{}catch(){} block surrounding the sleep method 
and try to execute the code. What is your observation? */
		 

/*Here I am getting the error because it is checked exception and we should handle it byusing throws or try catch
block.

*Exception in thread "Thread-0" java.lang.Error: Unresolved compilation problem: 
	Unhandled exception type InterruptedException

	at com.assessment6.ThreadDemo2.run(ThreadDemo2.java:17)*/
class ThreadDemo2 extends Thread{  
	  public void run(){  
		  Thread.currentThread().setName("t1");
		  System.out.println(Thread.currentThread().getName());
		  Thread.currentThread().setName("MyThread");
		  System.out.println(Thread.currentThread().getName());
		  System.out.println(LocalDate.now());
			
				Thread.sleep(10000);
	  }  
	 public static void main(String args[]){  
	  ThreadDemo2 t1=new ThreadDemo2(); 
	  t1.start();   
}  
}  