package com.assessment6;

/*4.Rewrite the earlier program so that, now the class DemoThread1 instead of implementing from Runnable interface,
will now extend from Thread class. */


class ThreadExam extends Thread{
	
	ThreadExam(){
		Thread t=new Thread();
		t.start();
	}
	public void run() 
	{		
		System.out.println("Running child thread in loop:");
		try {
            for(int i=1;i<=10;i++){ 
            	System.out.println(i);
                Thread.sleep(2000);
               }
      }catch(InterruptedException e){ 
    	  e.printStackTrace();
    	  System.out.println(e);
    }
	 }
  	}
  class DemoThread4{
	public static void main(String[] args) {
		ThreadExam dt=new ThreadExam();
		ThreadExam dt1=new ThreadExam();
		ThreadExam dt2=new ThreadExam();
		dt.start();
		dt1.start();
		dt2.start();
	}
		
	}  