package com.assessment6;

import java.io.InterruptedIOException;
import java.time.LocalDate;

/*1.Write a program to assign the current thread to t1.
Change the name of the thread to MyThread.Display the changed name of the thread. 
Also it should display the current time. Put the thread to sleep for 10 seconds and display the time again. 
 */

class ThreadDemo extends Thread{  
	  public void run(){  
		  Thread.currentThread().setName("t1");
		  System.out.println(Thread.currentThread().getName());
		  Thread.currentThread().setName("MyThread");
		  System.out.println(Thread.currentThread().getName());
		  System.out.println(LocalDate.now());
		  
			  try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.out.println(e);
			}
			  System.out.println(LocalDate.now());
	  }  
	 public static void main(String args[]){  
	  ThreadDemo t1=new ThreadDemo(); 
	  t1.start();   
  }  
}  